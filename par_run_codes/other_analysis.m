%%% Set the main folder path %%%
folder_path = 'ScratchStimuliTest';
addpath(genpath(folder_path))
folder_path = 'MAP';
addpath(genpath(folder_path))
folder_path = 'IR';
addpath(genpath(folder_path))
folder_path = 'fce_wavelet';
addpath(genpath(folder_path))
folder_path = 'fce_matched';
addpath(genpath(folder_path))
folder_path = 'fce_audmodel';
addpath(genpath(folder_path))
folder_path = 'fce_AR';
addpath(genpath(folder_path))
folder_path = 'Erblets';
addpath(genpath(folder_path))
folder_path = 'Aud_tbx';
addpath(genpath(folder_path))
folder_path = 'amtoolbox-full-1.2.0';
addpath(genpath(folder_path))

r =[1:5]
lth = [1,5,10,25,50]
BM_flags = [0,1]

al = cell(5,5,2);

test_results = struct();



%% INITIAL DATA
number_trials = 5;
% linear threshold division no 
% (selected ranges are in Initialization section below)
n = 50;

% Algorithm specific detection parameters
% DRNL
channels_drnl_first_band  = 7:18 ; % DRNL correlation channels 
% Erblet
med_len_erblet = 30:5:180; %median filter length
channel_start = 20:40 ;
% Lyon
channels_lyon_end = 25:55;
% Seneff 
channels_seneff_end = 8:24; %channels (bands) selection
med_len_seneff = 50:20:500; %median filter length
% AR
LengthSeg_ar = 256:256:8192; % be careful about the stationarity of the signal  
Order_ar = 8:2:40; 
% matched
LengthSeg_matched = 256:256:8192; % be careful about the stationarity of the signal  
Order_matched = 8:2:40; 
% Wavelet 
LengthFilt = 30:5:180; %Median filter window length L !
% BM
channel_begin_BM = round(logspace(log10(5),log10(100),8)); % cross correlation channels
channel_end_BM = round(logspace(log10(40),log10(150),8)); 
% Initialization
% Hard-set threshold ranges based-on previous experiments
%new models
th_drnl = linspace(10, 32, n);
th_erb = linspace(3, 10, n);
th_lyon = linspace(8, 22, n);
th_senef = linspace(2, 20, n);
%old models
th_ar = linspace(4, 32, n);
th_matched = linspace(5, 50, n);
th_wave = linspace(5, 50, n);
th_vencovs = linspace(25, 65, n);
% create a matrix of parameters 
% Gather all lists into a cell array
param_lists = {channels_drnl_first_band, med_len_erblet, channel_start, channels_lyon_end, ...
         channels_seneff_end, med_len_seneff, LengthSeg_ar, Order_ar, ...
         LengthSeg_matched, Order_matched, LengthFilt,channel_begin_BM,channel_end_BM};

param_len = length(param_lists);
% Find the length of the longest list
max_len = max(cellfun(@length, param_lists));

counter=0;
% load data
c = 0;
% Ap, dp test results
for listen_th = lth
    c = c+1;
    for i = r
        for j = BM_flags
            filename = ['par_results/output' num2str(i) '_lth' num2str(listen_th) '_BM' num2str(j) '.mat'];
                if exist(filename, 'file') == 0
                    % fprintf('The file %s does not exist.\n', filename);
                    a = [ 'qsub run' num2str(i) '_lth' num2str(listen_th) '_BM' num2str(j) '.sh'];
                    disp(a)
                else
                    al{i,c,j+1} = load(filename);
                    counter = counter+1;
                end
            
            
        end
    end
end


%% IF test_run.mat is available directly load it and skip above part
load test_run.mat
 
%% Time avaraging

et_names = {'et1', 'et2', 'et3', 'et4', 'et5', 'et6', 'et7', 'et8'};
algo_names = {"DRNL", "ERBlet", "Lyon's", "Seneff's", "AR", "Matched", "Wavelet", "Vencovsky's"};

load('subj_results.mat'); %load subjective test results

for k = 1:length(et_names)
c = 0;
sums = 0;
tots = 0;
    for listen_th = lth
        c = c+1;
        for i = r
                % Define the et matrix names
            if k ~= 8
                et_matrix = al{i, c, 1}.et.(et_names{k});
            else
                et_matrix = al{i, c, 2}.et.(et_names{k});
            end

         if i == 1
            train_list = [42, 51, 55, 5, 28, 75, 47, 89, 33, 58, 57, 31, 70, 85, 16, 53, 49, 17, 44, 39, 11, 45, 15, 8, 90, 37, 30, 25, 60, 34, 52, 7, 3, 35, 10, 6, 41, 86, 20, 9, 76, 82, 59, 80, 1, 63, 14, 19, 56, 62, 40, 12, 2, 29, 88, 48, 66, 64, 77, 71];
        elseif i == 2
            train_list = [21, 23, 62, 53, 51, 49, 61, 7, 82, 54, 29, 10, 63, 68, 78, 18, 56, 4, 26, 24, 47, 41, 20, 8, 2, 74, 71, 3, 28, 89, 60, 16, 9, 72, 67, 42, 44, 12, 27, 35, 5, 76, 52, 33, 14, 39, 58, 37, 69, 48, 80, 46, 55, 6, 25, 88, 73, 66, 13, 79];
        elseif i == 3
            train_list = [65, 7, 55, 87, 75, 51, 49, 63, 4, 14, 42, 30, 2, 56, 19, 46, 70, 90, 64, 32, 40, 58, 21, 35, 12, 76, 28, 3, 20, 33, 67, 44, 5, 57, 22, 38, 48, 86, 47, 54, 31, 60, 9, 16, 17, 1, 78, 41, 73, 10, 39, 80, 50, 66, 36, 24, 72, 13, 45, 68];
        elseif i == 4
            train_list = [90, 9, 87, 83, 79, 2, 46, 35, 4, 5, 72, 29, 50, 84, 6, 70, 56, 16, 55, 33, 27, 48, 63, 13, 53, 11, 28, 24, 18, 85, 17, 65, 41, 26, 8, 23, 1, 36, 78, 86, 77, 80, 22, 12, 68, 19, 81, 76, 73, 34, 71, 3, 14, 31, 66, 20, 38, 47, 52, 62];
        else
            train_list = [87, 67, 72, 61, 39, 29, 64, 49, 9, 77, 14, 37, 32, 48, 22, 34, 36, 21, 85, 50, 73, 17, 12, 58, 53, 86, 84, 31, 60, 6, 45, 19, 59, 68, 18, 35, 63, 78, 89, 13, 3, 16, 30, 52, 7, 8, 66, 76, 11, 69, 88, 82, 57, 20, 56, 70, 38, 71, 55, 51];
         end

        subjRes = vysledky(train_list,2)/100;
         idxHIT = find(subjRes>=(100-listen_th)/100); %marked as containing clicks > 75 %
        idxNO = find(subjRes<=listen_th/100); %marked as containing clicks < 75 %
        
        kstim = train_list(sort([idxHIT;idxNO])); %"Unsure" samples removed

            
            audio_time = length(kstim)*0.8;
            % Perform calculation
            sums = sums + sum(et_matrix(et_matrix > 0),"omitnan");
            tots = tots + sum(et_matrix(:) > 0);
            
        end            

    end
                % Display or store the results
            disp(['For ' algo_names{k} ':']);
            disp(['Number of elements greater than zero: ' num2str(tots)]);
            disp(['Average time: ' num2str(sums/tots)]);
            disp(['Real time: ' num2str((sums/tots)*100/audio_time)]);

            disp('---');
  t_avg(k) = sums/tots;
  t_real(k) = (sums/tots)*100/audio_time;
end


Ap_order = [5, 6, 8, 3, 7, 1, 4, 2];

x=1:8;
figure();
b=bar(x,log10(t_real(Ap_order)));
b.FaceColor = 'flat';
b.CData(1,:) = [.5 .5 .5];
b.CData(2,:) = [.5 .5 .5];
b.CData(3,:) = [.5 .5 .5];
b.CData(5,:) = [.5 .5 .5];
b.CData(4,:) = [.5 .5 .5];
b.CData(6,:) = [.5 .5 .5];
b.CData(7,:) = [.5 .5 .5];
b.CData(8,:) = [.5 .5 .5];

xlabel('Algorithms');
ylabel("T_R_T (log_1_0)");
title("T_R_T in log_1_0 scale");
xticks(x);
xticklabels(algo_names([5, 6, 8, 3, 7, 1, 4, 2]));


%% Erblet H and F graph
erblet_cd = al{3,3,1}.cod.cd2;
erblet_fd = al{3,3,1}.fd.fd2;

% %
% drnl_cd = al{3,3,1}.cod.cd1;
% drnl_fd = al{3,3,1}.fd.fd1;
% 
% lyon_cd = al{3,3,1}.cod.cd3;
% lyon_fd = al{3,3,1}.fd.fd3;
% 
% senef_cd = al{3,3,1}.cod.cd4;
% senef_fd = al{3,3,1}.fd.fd4;


figure();

% erblet
y_cd = erblet_cd(:, 15, 10);
y_fd = erblet_fd(:, 15, 10);
indices_cd = find(y_cd >= 0);
indices_fd = find(y_fd >= 0);
y_cd = y_cd(indices_cd);
y_fd = y_fd(indices_fd);

% % drnl
% y_cd1 = drnl_cd(:, 1, 1);
% y_fd1 = drnl_fd(:, 1, 1);
% indices_cd1 = find(y_cd1 >= 0);
% indices_fd1 = find(y_fd1 >= 0);
% y_cd1 = y_cd1(indices_cd1);
% y_fd1 = y_fd1(indices_fd1);
% 
% % lyon
% y_cd3 = lyon_cd(:, 15, 1);
% y_fd3 = lyon_fd(:, 15, 1);
% indices_cd3 = find(y_cd3 >= 0);
% indices_fd3 = find(y_fd3 >= 0);
% y_cd3 = y_cd3(indices_cd3);
% y_fd3 = y_fd3(indices_fd3);
% 
% % seneff
% y_cd4 = senef_cd(:, 15, 10);
% y_fd4 = senef_fd(:, 15, 10);
% indices_cd4 = find(y_cd4 >= 0);
% indices_fd4 = find(y_fd4 >= 0);
% y_cd4 = y_cd4(indices_cd4);
% y_fd4 = y_fd4(indices_fd4);

 
% g_cd = fit(x', y_cd, 'gauss1');
% g_fd = fit(x', y_fd, 'gauss1');

% Plotting for subplot 1
subplot(3,1,1);
hold on;

plot(y_cd, 'Color', 'blue', 'LineWidth', 1.5); %erblet
plot(y_fd, 'Color', 'red', 'LineWidth', 1.5);

% 
% plot(y_cd, 'Color', 'blue', 'LineWidth', 1.5); %erblet
% plot(y_fd,'-*', 'Color', 'blue', 'LineWidth', 1.5);
% 
% plot(y_cd1, 'Color', 'red', 'LineWidth', 1.5); %drnl
% plot(y_fd1,'-*', 'Color', 'red', 'LineWidth', 1.5);
% 
% plot(y_cd3, 'Color', 'green', 'LineWidth', 1.5); % lyon
% plot(y_fd3,'-*', 'Color', 'green', 'LineWidth', 1.5);
% 
% plot(y_cd4, 'Color', 'magenta', 'LineWidth', 1.5); % Seneff
% plot(y_fd4,'-*', 'Color', 'magenta', 'LineWidth', 1.5);


title('H and F across threshold gain parameter range');
ylabel('Ratio (%)');
legend('ERBlet H','ERBlet F');
% legend('ERBlet H','ERBlet F','DRNL H','DRNL F','Lyon H','Lyon F', 'Seneff H', 'Seneff F');

% Plotting for subplot 2
subplot(3,1,2);
% 
% % erblet
% y_cd = erblet_cd(15, :, 10);
% y_fd = erblet_fd(15, :, 10);
% indices_cd = find(y_cd >= 0);
% indices_fd = find(y_fd >= 0);
% y_cd = y_cd(indices_cd);
% y_fd = y_fd(indices_fd);
% 
% % drnl
% y_cd1 = drnl_cd(15, :, 1);
% y_fd1 = drnl_fd(15, :, 1);
% indices_cd1 = find(y_cd1 >= 0);
% indices_fd1 = find(y_fd1 >= 0);
% y_cd1 = y_cd1(indices_cd1);
% y_fd1 = y_fd1(indices_fd1);
% 
% % lyon
% y_cd3 = lyon_cd(15, :, 1);
% y_fd3 = lyon_fd(15, :, 1);
% indices_cd3 = find(y_cd3 >= 0);
% indices_fd3 = find(y_fd3 >= 0);
% y_cd3 = y_cd3(indices_cd3);
% y_fd3 = y_fd3(indices_fd3);
% 
% % seneff
% y_cd4 = senef_cd(15, :, 10);
% y_fd4 = senef_fd(15, :, 10);
% indices_cd4 = find(y_cd4 >= 0);
% indices_fd4 = find(y_fd4 >= 0);
% y_cd4 = y_cd4(indices_cd4);
% y_fd4 = y_fd4(indices_fd4);
% % y_cd = erblet_cd(25, :, 10);
% % y_fd = erblet_fd(25, :, 10);

hold on;

plot(y_cd, 'Color', 'blue', 'LineWidth', 1.5); %erblet
plot(y_fd, 'Color', 'red', 'LineWidth', 1.5);

% plot(y_cd, 'Color', 'blue', 'LineWidth', 1.5); %erblet
% plot(y_fd,'-*', 'Color', 'blue', 'LineWidth', 1.5);
% 
% plot(y_cd1, 'Color', 'red', 'LineWidth', 1.5); %drnl
% plot(y_fd1,'-*', 'Color', 'red', 'LineWidth', 1.5);
% 
% plot(y_cd3, 'Color', 'green', 'LineWidth', 1.5); % lyon
% plot(y_fd3,'-*', 'Color', 'green', 'LineWidth', 1.5);
% 
% plot(y_cd4, 'Color', 'magenta', 'LineWidth', 1.5); % Seneff
% plot(y_fd4,'-*', 'Color', 'magenta', 'LineWidth', 1.5);

title('H and F across first distinct parameter');
ylabel('Ratio (%)');
legend('ERBlet H','ERBlet F');

% legend('ERBlet H','ERBlet F','DRNL H','DRNL F','Lyon H','Lyon F', 'Seneff H', 'Seneff F');
xlabel('Parameter Range');

% Plotting for subplot 3
subplot(3,1,3);

% erblet
y_cd = reshape(erblet_cd(15, 15, :),1,[]);
y_fd = reshape(erblet_fd(15, 15, :),1,[]);
indices_cd = find(y_cd >= 0);
indices_fd = find(y_fd >= 0);
y_cd = y_cd(indices_cd);
y_fd = y_fd(indices_fd);

% % seneff
% y_cd4 = reshape(senef_cd(15, 15, :),1,[]);
% y_fd4 = reshape(senef_fd(15, 15, :),1,[]);
% indices_cd4 = find(y_cd4 >= 0);
% indices_fd4 = find(y_fd4 >= 0);
% y_cd4 = y_cd4(indices_cd4);
% y_fd4 = y_fd4(indices_fd4);
% % y_cd = erblet_cd(25, :, 10);
% % y_fd = erblet_fd(25, :, 10);

hold on;
plot(y_cd, 'Color', 'blue', 'LineWidth', 1.5); %erblet
plot(y_fd, 'Color', 'red', 'LineWidth', 1.5);


% plot(y_cd, 'Color', 'blue', 'LineWidth', 1.5); %erblet
% plot(y_fd,'-*', 'Color', 'blue', 'LineWidth', 1.5);

% plot(y_cd4, 'Color', 'magenta', 'LineWidth', 1.5); % Seneff
% plot(y_fd4,'-*', 'Color', 'magenta', 'LineWidth', 1.5);


title('H and F across second distinct parameter');
ylabel('Ratio (%)');
legend('ERBlet H','ERBlet F');

% legend('ERBlet H','ERBlet F', 'Seneff H', 'Seneff F');
xlabel('Parameter Range');


hold off;

% Save the figure as EPS
filename = 'erbletHF.eps';
print(gcf, filename, '-depsc', '-r300'); % Use '-r' to specify the resolution (dots per inch), adjust as needed



%% ROC

erblet_cd = al{3,3,1}.cod.cd2;
% Initialize a cell array to store algorithm names
algorithm_names = {"DRNL", "ERBlet", "Lyon's", "Seneff's", "AR", "Match", "Wavelet", "Vencovsky's", "Diagonal"};
plot_list = [1, 2, 3, 4];
% Plotting
figure();
hold on;

for algorithm_index = 1:8
    
    if algorithm_index ~= 8
    % Accessing data for the current algorithm
    current_algorithm_cd = al{3, 3, 1}.cod.(['cd' num2str(algorithm_index)]);
    positive_values = current_algorithm_cd(current_algorithm_cd > 0);
    current_algorithm_fd = al{3, 3, 1}.fd.(['fd' num2str(algorithm_index)]);
     current_algorithm_fd= current_algorithm_fd( current_algorithm_cd>0);
      current_algorithm_cd = positive_values;
    else
     % Accessing data for the current algorithm
    current_algorithm_cd = al{3, 3, 2}.cod.(['cd' num2str(algorithm_index)]);
    positive_values = current_algorithm_cd(current_algorithm_cd > 0);
    current_algorithm_fd = al{3, 3, 2}.fd.(['fd' num2str(algorithm_index)]);
     current_algorithm_fd= current_algorithm_fd( current_algorithm_cd>0);
      current_algorithm_cd = positive_values;
    end
    % Reshape data
    y_cd = reshape(current_algorithm_cd, 1, numel(current_algorithm_cd));
    y_fd = reshape(current_algorithm_fd, 1, numel(current_algorithm_fd));
    
    % Uncomment the next two lines if you want to smooth the data
    y_cd = smoothdata(y_cd, 'movmean', 50);
    y_fd = smoothdata(y_fd, 'movmean', 50);
    
    [y_cd,idx] = sort(y_cd);
    [y_fd,idx] = sort(y_fd);

    isInList = ismember(algorithm_index, plot_list);

    % Plotting with different colors for each algorithm
    if isInList
        plot(y_fd,y_cd','LineWidth',4);
    end
end
diag = 1:100;
plot(diag,diag',"--",'LineWidth',4)
% Add labels, legend, and title
ylabel('H ratio');
xlabel('F ratio');
title('ROC curves');
% legend("ERBlet","Seneff's","Diagonal")
legend(algorithm_names{[plot_list,9]})


% grid on;
hold off;

% Save the figure as EPS
filename = 'ROC.eps';
print(gcf, filename, '-depsc', '-r300'); % Use '-r' to specify the resolution (dots per inch), adjust as needed


% Uncomment the next line if you want to set the axis limits
% axis([xmin xmax ymin ymax]);




    % 1.DRNL
    % 2.Erblet detection
    % 3.Lyon
    % 4.Seneff
    % 5.Analysis AR
    % 6.Analysismatch
    % 7. Wavelet analysis
    % 8. BMmodelTEST

%% Best resulting parameter extraction

%best thresholds
drnl1 = reshape(best_th(1,:,:,1),1,25)
erb1 = reshape(best_th(2,:,:,1),1,25)
lyon1 = reshape(best_th(3,:,:,1),1,25)
senef1 = reshape(best_th(4,:,:,1),1,25)
ar1 = reshape(best_th(5,:,:,1),1,25)
match1 = reshape(best_th(6,:,:,1),1,25)
wave1 = reshape(best_th(7,:,:,1),1,25)
bm1 = reshape(best_th(8,:,:,1),1,25)

% best parameter 1
drnl2 = reshape(best_p1(1,:,:,1),1,25)
erb2 = reshape(best_p1(2,:,:,1),1,25)
lyon2 = reshape(best_p1(3,:,:,1),1,25)
senef2 = reshape(best_p1(4,:,:,1),1,25)
ar2 = reshape(best_p1(5,:,:,1),1,25)
match2 = reshape(best_p1(6,:,:,1),1,25)
wave2 = reshape(best_p1(7,:,:,1),1,25)
bm2 = reshape(best_p1(8,:,:,1),1,25)

% best parameter 2
erb3 = reshape(best_p2(2,:,:,1),1,25)
senef3 = reshape(best_p2(4,:,:,1),1,25)
ar3 = reshape(best_p2(5,:,:,1),1,25)
match3 = reshape(best_p2(6,:,:,1),1,25)
bm3 = reshape(best_p2(8,:,:,1),1,25)

drnl_ap = reshape(max_train(1,:,:,1),1,25)
erb_ap = reshape(max_train(2,:,:,1),1,25)
lyon_ap = reshape(max_train(3,:,:,1),1,25)
senef_ap = reshape(max_train(4,:,:,1),1,25)
ar_ap = reshape(max_train(5,:,:,1),1,25)
match_ap = reshape(max_train(6,:,:,1),1,25)
wave_ap = reshape(max_train(7,:,:,1),1,25)
bm_ap = reshape(max_train(8,:,:,1),1,25)




