% Impulse noise detection using ear model
% evaluation of the detection results
% Authors: Marek Semansky, Frantisek Rund
function [res_perc, false_perc, dprime, Ap] = evaluation(det, hit, false_hit, vysledky)
% disp(det)
% disp(hit)
% disp(false_hit)
res_perc = sum(det(hit==1))/sum(hit)*100;
false_perc = sum(det(false_hit==1))/sum(false_hit)*100;
dprime=(norminv(res_perc/100)-norminv(false_perc/100));

cd = res_perc/100;
fd = false_perc /100;
Ap = 0.5 + sign(cd-fd)*((cd-fd)^2 + abs(cd-fd))/(4*max(cd,fd)-4*cd*fd);
