% DRNL model z MAP toolboxu
% Autor: Marek Semansk�
% POZOR! BFlist je pevn� d�n logaritmicky

function [DRNLoutput] = DRNL_MAP1_14(inputSignal, sampleRate)
%% BM parametry z MAPparamsNormal.m
efferentDelay=0.010;
OMEParams=[];  % clear the structure first
% outer ear resonances band pass filter  [gain lp order  hp]
OMEParams.externalResonanceFilters=      [ 10 1 1000 4000];
                                       
% highpass stapes filter  
%  Huber gives 2e-9 m at 80 dB and 1 kHz (2e-13 at 0 dB SPL)
OMEParams.OMEstapesHPcutoff= 1000;
OMEParams.stapesScalar=	     45e-9;

% Acoustic reflex: maximum attenuation should be around 25 dB (Price, 1966)
% i.e. a minimum ratio of 0.056.
% 'spikes' model: AR based on brainstem spiking activity (LSR)
OMEParams.rateToAttenuationFactor=0.05; % * N(all ICspikes)
% 'probability model': Ar based on AN firing probabilities (LSR)
OMEParams.rateToAttenuationFactorProb=0.02;    % * N(all ANrates)

% asymptote should be around 100-200 ms
OMEParams.ARtau=.250; % AR smoothing function 250 ms fits Hung and Dallos
% delay must be longer than the segment length
OMEParams.ARdelay=efferentDelay;  %Moss gives 8.5 ms latency
OMEParams.ARrateThreshold=40;
%%  #3 DRNL
DRNLParams=[];  % clear the structure first
% DRNLParams.BFlist=BFlist;
lowestBF=250; 	highestBF= 8000; 	numChannels=21;
BFlist=round(logspace(log10(lowestBF),log10(highestBF),numChannels));

%   *** DRNL nonlinear path
% broken stick compression
DRNLParams.a=2e4;       % DRNL.a=0 means no OHCs (no nonlinear path)
DRNLParams.c=.2;        % compression exponent
DRNLParams.ctBMdB = 10; %Compression threshold dB re 10e-9 m displacement

% filters
DRNLParams.nonlinOrder=	3;  % order of nonlinear gammatone filters
DRNLParams.nonlinCFs=BFlist;
DRNLParams.p=0.2895;   DRNLParams.q=250;   % save p and q for printing only
% p=0.2895;   q=250;      % human  (% p=0.14;   q=366;  % cat)
DRNLParams.nlBWs=  DRNLParams.p * BFlist + DRNLParams.q;

%   *** DRNL linear path:
DRNLParams.g=100;       % linear path gain factor
DRNLParams.linOrder=3;  % order of linear gammatone filters
% linCF is not necessarily the same as nonlinCF
minLinCF=153.13; coeffLinCF=0.7341;   % linCF>nonlinBF for BF < 1 kHz
DRNLParams.linCFs=minLinCF+coeffLinCF*BFlist;
% bandwidths (linear)
minLinBW=100; coeffLinBW=0.6531;
DRNLParams.linBWs=minLinBW + coeffLinBW*BFlist; % bandwidths of linear  filters

%   *** DRNL MOC efferents

DRNLParams.MOCdelay = efferentDelay;            % must be < segment length!
DRNLParams.minMOCattenuationdB=-35;

% 'spikes' model: MOC based on brainstem spiking activity (HSR)
DRNLParams.MOCtau =.0285;                         % smoothing for MOC
DRNLParams.rateToAttenuationFactor = .03;  % strength of MOC
DRNLParams.rateToAttenuationFactor = .0055;  % strength of MOC

% 'probability' model: MOC based on AN probability (HSR)
DRNLParams.MOCtauProb =.285;                         % smoothing for MOC
DRNLParams.rateToAttenuationFactorProb = 0.007;  % strength of MOC
DRNLParams.MOCrateThresholdProb =67;                % spikes/s probability only

%% zacatek MAP1_14.m
restorePath=path;
% addpath (['..' filesep 'parameterStore'])

% global OMEParams DRNLParams IHC_cilia_RPParams IHCpreSynapseParams
% global AN_IHCsynapseParams MacGregorParams MacGregorMultiParams

% All of the results of this function are stored as global
global  savedParamChanges savedBFlist saveAN_spikesOrProbability ...
    saveMAPparamsName savedInputSignal dt dtSpikes ...
    OMEextEarPressure TMoutput OMEoutput DRNLoutput...
    IHC_cilia_output IHCrestingCiliaCond IHCrestingV...
    IHCoutput ANprobRateOutput ANoutput savePavailable saveNavailable ...
    ANtauCas CNtauGk CNoutput ICoutput ICmembraneOutput ICfiberTypeRates...
    MOCattenuation ARattenuation 


% Normally only ICoutput(logical spike matrix) or ANprobRateOutput will be
% needed by the user; so the following will suffice
%   global dtSpikes ICoutput ANprobRateOutput

% Note that sampleRate has not changed from the original function call and
%  ANprobRateOutput is sampled at this rate
% However ANoutput, CNoutput and IC output are stored as logical
%  'spike' matrices using a lower sample rate (see dtSpikes).

% When AN_spikesOrProbability is set to probability,
%  no spike matrices are computed.
% When AN_spikesOrProbability is set to 'spikes',
%  no probability output is computed

% Efferent control variables are ARattenuation and MOCattenuation
%  These are scalars between 1 (no attenuation) and 0.
%  They are represented with dt=1/sampleRate (not dtSpikes)
%  They are computed using either AN probability rate output
%   or IC (spikes) output as approrpriate.
% AR is computed using across channel activity
% MOC is computed on a within-channel basis.

if nargin<1
    error(' MAP1_14 is not a script but a function that must be called')
end

if nargin<6
    paramChanges=[];
end
% Read parameters from MAPparams<***> file in 'parameterStore' folder
% Beware, 'BFlist=-1' is a legitimate argument for MAPparams<>
%  It means that the calling program allows MAPparams to specify the list
% cmd=['method=MAPparams' MAPparamsName ...
%     '(BFlist, sampleRate, 0, paramChanges);'];
% eval(cmd);
BFlist=DRNLParams.nonlinCFs;

% save as global for later plotting if required
% savedBFlist=BFlist;
% saveAN_spikesOrProbability=AN_spikesOrProbability;
% saveMAPparamsName=MAPparamsName;
% savedParamChanges=paramChanges;

dt=1/sampleRate;
duration=length(inputSignal)/sampleRate;
% segmentDuration is specified in parameter file (must be >efferent delay)
method.segmentDuration=efferentDelay;
segmentDuration=method.segmentDuration;
segmentLength=round(segmentDuration/ dt);
segmentTime=dt*(1:segmentLength); % used in debugging plots
AN_IHCsynapseParams.spikesTargetSampleRate=10000;

% all spiking activity is computed using longer epochs
ANspeedUpFactor=ceil(sampleRate/AN_IHCsynapseParams.spikesTargetSampleRate);
% ANspeedUpFactor=AN_IHCsynapseParams.ANspeedUpFactor;  % e.g.5 times

% inputSignal must be  row vector
[r c]=size(inputSignal);
if r>c, inputSignal=inputSignal'; end       % transpose
% ignore stereo signals
inputSignal=inputSignal(1,:);               % drop any second channel
savedInputSignal=inputSignal;

% Segment the signal
% The sgment length is given but the signal length must be adjusted to be a
% multiple of both the segment length and the reduced segmentlength
[nSignalRows signalLength]=size(inputSignal);
segmentLength=ceil(segmentLength/ANspeedUpFactor)*ANspeedUpFactor;
% Make the signal length a whole multiple of the segment length
nSignalSegments=ceil(signalLength/segmentLength);
padSize=nSignalSegments*segmentLength-signalLength;
pad=zeros(nSignalRows,padSize);
inputSignal=[inputSignal pad];
[ignore signalLength]=size(inputSignal);

% spiking activity is computed at longer sampling intervals (dtSpikes)
%  so it has a smaller number of epochs per segment(see 'ANspeeUpFactor' above)
% AN CN and IC all use this sample interval
dtSpikes=dt*ANspeedUpFactor;
reducedSegmentLength=round(segmentLength/ANspeedUpFactor);
reducedSignalLength= round(signalLength/ANspeedUpFactor);

%% Initialise with respect to each stage before computing
%  by allocating memory,
%  by computing constants
%  by establishing easy to read variable names
% The computations are made in segments and boundary conditions must
%  be established and stored. These are found in variables with
%  'boundary' or 'bndry' in the name

%% OME ---
% external ear resonances
OMEexternalResonanceFilters=OMEParams.externalResonanceFilters;
[nOMEExtFilters c]=size(OMEexternalResonanceFilters);
% details of external (outer ear) resonances
OMEgaindBs=OMEexternalResonanceFilters(:,1);
OMEgainScalars=10.^(OMEgaindBs/20);
OMEfilterOrder=OMEexternalResonanceFilters(:,2);
OMElowerCutOff=OMEexternalResonanceFilters(:,3);
OMEupperCutOff=OMEexternalResonanceFilters(:,4);
% external resonance coefficients
ExtFilter_b=cell(nOMEExtFilters,1);
ExtFilter_a=cell(nOMEExtFilters,1);
for idx=1:nOMEExtFilters
    Nyquist=sampleRate/2;
    [b, a] = butter(OMEfilterOrder(idx), ...
        [OMElowerCutOff(idx) OMEupperCutOff(idx)]...
        /Nyquist);
    ExtFilter_b{idx}=b;
    ExtFilter_a{idx}=a;
end
OMEExtFilterBndry=cell(2,1);
OMEextEarPressure=zeros(1,signalLength); % pressure at tympanic membrane

% pressure to velocity conversion using smoothing filter (50 Hz cutoff)
tau=1/(2*pi*50);
a1=dt/tau-1; a0=1;
b0=1+ a1;
TMdisp_b=b0; TMdisp_a=[a0 a1];
% figure(9), freqz(TMdisp_b, TMdisp_a)
OME_TMdisplacementBndry=[];

% OME high pass (simulates poor low frequency stapes response)
OMEhighPassHighCutOff=OMEParams.OMEstapesHPcutoff;
Nyquist=sampleRate/2;
[stapesDisp_b,stapesDisp_a] = butter(1, OMEhighPassHighCutOff/Nyquist, 'high');
% figure(10), freqz(stapesDisp_b, stapesDisp_a)

OMEhighPassBndry=[];

% OMEampStapes might be reducdant (use OMEParams.stapesScalar)
stapesScalar= OMEParams.stapesScalar;

% Acoustic reflex
efferentDelayPts=round(OMEParams.ARdelay/dt);
% smoothing filter
a1=dt/OMEParams.ARtau-1; a0=1;
b0=1+ a1;
ARfilt_b=b0; ARfilt_a=[a0 a1];

ARattenuation=ones(1,signalLength);
ARrateThreshold=OMEParams.ARrateThreshold; % may not be used
ARrateToAttenuationFactor=OMEParams.rateToAttenuationFactor;
ARrateToAttenuationFactorProb=OMEParams.rateToAttenuationFactorProb;
ARboundary=[];
ARboundaryProb=0;

% save complete OME record (stapes displacement)
OMEoutput=zeros(1,signalLength);
TMoutput=zeros(1,signalLength);

%% BM ---
% BM is represented as a list of locations identified by BF
DRNL_BFs=BFlist;
nBFs= length(DRNL_BFs);

% DRNLchannelParameters=DRNLParams.channelParameters;
DRNLresponse= zeros(nBFs, segmentLength);

MOCrateToAttenuationFactor=DRNLParams.rateToAttenuationFactor;
rateToAttenuationFactorProb=DRNLParams.rateToAttenuationFactorProb;
MOCrateThresholdProb=DRNLParams.MOCrateThresholdProb;
minMOCattenuation=10^(DRNLParams.minMOCattenuationdB/20);

% smoothing filter for MOC
a1=dt/DRNLParams.MOCtau-1; a0=1;
b0=1+ a1;
MOCfilt_b=b0; MOCfilt_a=[a0 a1];

a1=dt/DRNLParams.MOCtauProb-1; a0=1;
b0=1+ a1;
MOCfiltProb_b=b0; MOCfiltProb_a=[a0 a1];


% figure(9), freqz(stapesDisp_b, stapesDisp_a)
MOCboundary=cell(nBFs,1);
MOCprobBoundary=cell(nBFs,1);

MOCattSegment=zeros(nBFs,reducedSegmentLength);
MOCattenuation=ones(nBFs,signalLength);

% if DRNLParams.a>0
%     DRNLcompressionThreshold=10^((1/(1-DRNLParams.c))* ...
%     log10(DRNLParams.b/DRNLParams.a));
% else
%     DRNLcompressionThreshold=inf;
% end
% DRNLcompressionThreshold=DRNLParams.cTh;
DRNLlinearOrder= DRNLParams.linOrder;
DRNLnonlinearOrder= DRNLParams.nonlinOrder;

DRNLa=DRNLParams.a;
% DRNLa2=DRNLParams.a2;
% DRNLb=DRNLParams.b;
DRNLc=DRNLParams.c;
linGAIN=DRNLParams.g;
ctBM=10e-9*10^(DRNLParams.ctBMdB/20);
CtS=ctBM/DRNLa;
%
% gammatone filter coefficients for linear pathway
bw=DRNLParams.linBWs';
phi = 2 * pi * bw * dt;
cf=DRNLParams.linCFs';
theta = 2 * pi * cf * dt;
cos_theta = cos(theta);
sin_theta = sin(theta);
alpha = -exp(-phi).* cos_theta;
b0 = ones(nBFs,1);
b1 = 2 * alpha;
b2 = exp(-2 * phi);
z1 = (1 + alpha .* cos_theta) - (alpha .* sin_theta) * i;
z2 = (1 + b1 .* cos_theta) - (b1 .* sin_theta) * i;
z3 = (b2 .* cos(2 * theta)) - (b2 .* sin(2 * theta)) * i;
tf = (z2 + z3) ./ z1;
a0 = abs(tf);
a1 = alpha .* a0;
GTlin_a = [b0, b1, b2];
GTlin_b = [a0, a1];
GTlinOrder=DRNLlinearOrder;
GTlinBdry=cell(nBFs,GTlinOrder);

% nonlinear gammatone filter coefficients 
bw=DRNLParams.nlBWs';
phi = 2 * pi * bw * dt;
cf=DRNLParams.nonlinCFs';
theta = 2 * pi * cf * dt;
cos_theta = cos(theta);
sin_theta = sin(theta);
alpha = -exp(-phi).* cos_theta;
b0 = ones(nBFs,1);
b1 = 2 * alpha;
b2 = exp(-2 * phi);
z1 = (1 + alpha .* cos_theta) - (alpha .* sin_theta) * i;
z2 = (1 + b1 .* cos_theta) - (b1 .* sin_theta) * i;
z3 = (b2 .* cos(2 * theta)) - (b2 .* sin(2 * theta)) * i;
tf = (z2 + z3) ./ z1;
a0 = abs(tf);
a1 = alpha .* a0;
GTnonlin_a = [b0, b1, b2];
GTnonlin_b = [a0, a1];
GTnonlinOrder=DRNLnonlinearOrder;
GTnonlinBdry1=cell(nBFs, GTnonlinOrder);
GTnonlinBdry2=cell(nBFs, GTnonlinOrder);

% complete BM record (BM displacement)
DRNLoutput=zeros(nBFs, signalLength);
%% Main program %%  %%  %%  %%  %%  %%  %%  %%  %%  %%  %%  %%  %%  %%

%  Compute the entire model for each segment
segmentStartPTR=1;
reducedSegmentPTR=1; % when sampling rate is reduced
while segmentStartPTR<signalLength
    segmentEndPTR=segmentStartPTR+segmentLength-1;
    % shorter segments after speed up.
    shorterSegmentEndPTR=reducedSegmentPTR+reducedSegmentLength-1;

    inputPressureSegment=inputSignal...
        (:,segmentStartPTR:segmentStartPTR+segmentLength-1);

    % segment debugging plots
    % figure(98)
    % plot(segmentTime,inputPressureSegment), title('signalSegment')


    % OME ----------------------

    % OME Stage 1: external resonances. Add to inputSignal pressure wave
    y=inputPressureSegment;
    
    for n=1:nOMEExtFilters
        % any number of resonances can be used
        [x  OMEExtFilterBndry{n}] = ...
            filter(ExtFilter_b{n},ExtFilter_a{n},...
            inputPressureSegment, OMEExtFilterBndry{n});
        x= x* OMEgainScalars(n);
        % This is a parallel resonance so add it
        y=y+x;
    end
%%
    inputPressureSegment=y;
    OMEextEarPressure(segmentStartPTR:segmentEndPTR)= inputPressureSegment;
    
    % OME stage 2: convert input pressure (velocity) to
    %  tympanic membrane(TM) displacement using low pass filter
    [TMdisplacementSegment  OME_TMdisplacementBndry] = ...
        filter(TMdisp_b,TMdisp_a,inputPressureSegment, ...
        OME_TMdisplacementBndry);
    % and save it
    TMoutput(segmentStartPTR:segmentEndPTR)= TMdisplacementSegment;

    % OME stage 3: middle ear high pass effect to simulate stapes inertia
    [stapesDisplacement  OMEhighPassBndry] = ...
        filter(stapesDisp_b,stapesDisp_a,TMdisplacementSegment, ...
        OMEhighPassBndry);

    % OME stage 4:  apply stapes scalar
    stapesDisplacement=stapesDisplacement*stapesScalar;

    % OME stage 5:    acoustic reflex stapes attenuation
    %  Attenuate the TM response using feedback from LSR fiber activity
    if segmentStartPTR>efferentDelayPts
        stapesDisplacement= stapesDisplacement.*...
            ARattenuation(segmentStartPTR-efferentDelayPts:...
            segmentEndPTR-efferentDelayPts);
    end

    % segment debugging plots
    % figure(98)
    % plot(segmentTime, stapesDisplacement), title ('stapesDisplacement')

    % and save
    OMEoutput(segmentStartPTR:segmentEndPTR)= stapesDisplacement;


    %% BM ------------------------------
    % Each location is computed separately
    efferentDelayPts=round(efferentDelay/dt);
    for BFno=1:nBFs

        %            *linear* path
        linOutput = stapesDisplacement * linGAIN;  % linear gain
       
        for order = 1 : GTlinOrder
            [linOutput GTlinBdry{BFno,order}] = ...
                filter(GTlin_b(BFno,:), GTlin_a(BFno,:), linOutput, ...
                GTlinBdry{BFno,order});
        end

        %           *nonLinear* path
        % efferent attenuation (0 <> 1)
        if segmentStartPTR>efferentDelayPts
            MOC=MOCattenuation(BFno, segmentStartPTR-efferentDelayPts:...
                segmentEndPTR-efferentDelayPts);
        else    % no MOC available yet
            MOC=ones(1, segmentLength);
        end
        % apply MOC to nonlinear input function      
%         figure(88), plot(MOC)
        nonlinOutput=stapesDisplacement.* MOC;

        %       first gammatone filter (nonlin path)
        for order = 1 : GTnonlinOrder
            [nonlinOutput GTnonlinBdry1{BFno,order}] = ...
                filter(GTnonlin_b(BFno,:), GTnonlin_a(BFno,:), ...
                nonlinOutput, GTnonlinBdry1{BFno,order});
        end
        
        
        % Nick's compression algorithm
        abs_x= abs(nonlinOutput);
        signs= sign(nonlinOutput);
        belowThreshold= abs_x<CtS;
        nonlinOutput(belowThreshold)= DRNLa *nonlinOutput(belowThreshold);
        aboveThreshold=~belowThreshold;
        nonlinOutput(aboveThreshold)= signs(aboveThreshold) *ctBM .* ...
            exp(DRNLc *log( DRNLa*abs_x(aboveThreshold)/ctBM ));
                       
        
%         %    original   broken stick instantaneous compression
%         holdY=nonlinOutput;
%         abs_x = abs(nonlinOutput);
%         nonlinOutput=sign(nonlinOutput).*min(DRNLa*abs_x, DRNLb*abs_x.^DRNLc);
% 

%         %   new broken stick instantaneous compression
%         y= nonlinOutput.* DRNLa;  % linear section attenuation/gain.
%         % compress parts of the signal above the compression threshold
%         %         holdY=y;
%         abs_y = abs(y);
%         idx=find(abs_y>DRNLcompressionThreshold);
%         if ~isempty(idx)>0
%             %             y(idx)=sign(y(idx)).* (DRNLcompressionThreshold + ...
%             %                 (abs_y(idx)-DRNLcompressionThreshold).^DRNLc);
%             y(idx)=sign(y(idx)).* (DRNLcompressionThreshold + ...
%                 (abs_y(idx)-DRNLcompressionThreshold)*DRNLa2);
%         end
%         nonlinOutput=y;


% if segmentStartPTR==10*segmentLength+1
%     figure(90)
%     plot(holdY,'b'), hold on
%     plot(nonlinOutput, 'r'), hold off
%     ylim([-1e-5 1e-5])
%     pause(1)
% end

%       second filter removes distortion products
        for order = 1 : GTnonlinOrder
            [ nonlinOutput GTnonlinBdry2{BFno,order}] = ...
                filter(GTnonlin_b(BFno,:), GTnonlin_a(BFno,:), ...
                nonlinOutput, GTnonlinBdry2{BFno,order});
        end

        %  combine the two paths to give the DRNL displacement
        DRNLresponse(BFno,:)=linOutput+nonlinOutput;
%         disp(num2str(max(linOutput)))
    end % BF

    % segment debugging plots
    % figure(98)
    %     if size(DRNLresponse,1)>3
    %         imagesc(DRNLresponse)  % matrix display
    %         title('DRNLresponse'); 
    %     else
    %         plot(segmentTime, DRNLresponse)
    %     end

    % and save it
    DRNLoutput(:, segmentStartPTR:segmentEndPTR)= DRNLresponse;
    segmentStartPTR=segmentStartPTR+segmentLength;
end