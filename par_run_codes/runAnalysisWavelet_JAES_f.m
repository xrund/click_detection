% Wavelet based click-detection test
% Authors: V�clav Vencovsk�, Marek Semansk�, Frantisek Rund
% FEE CTU 2021
% MATLAB Version: 9.4.0.813654 (R2018a)

function [Ap,dprime,CD, FA,elapsed_time] = runAnalysisWavelet_JAES_f(threshold_gain, LengthFilt, sig_list,listen_th)

%clear
addpath('fce_wavelet');  % add path to folder with wavelet click detection functions

%sig_list = list of signals to be used
load('subj_results.mat');

%subjective test results loading
subjRes = vysledky(sig_list,2)/100;

idxHIT = find(subjRes >= (100-listen_th)/100); %marked as containing clicks > 75 %
idxNO = find(subjRes<=listen_th/100); %marked as containing clicks < 75 %

kstim = sig_list(sort([idxHIT;idxNO])); %"Unsure" samples removed

%Folder with the samples
FolderName = 'ScratchStimuliTest/';
listWav = dir(FolderName);


%detection parameters
% LengthFilt = 150; %Median filter window length L !
Threshold = threshold_gain % 25; %Detection threshold gain K !
vlnka = 'db30'; %wavelet type and order

idxLup = 17640; % Starting position for the detection - half of the signal (sample) length
idxTol = idxLup-1; % Range for click detection - half of the signal (sample) length - a click is searched in full length of the sample


clickWM = zeros(length(listWav)-2,1); %preallocation of detection vector

        
disp(['Median filter window L: ' num2str(LengthFilt) ', Detection threshold parameter K: ' num2str(Threshold) ', Wavelet: ' num2str(vlnka)]) %zobrazeni parametru detekce

tic

    for k=1:length(kstim) %detection cycle
    disp(['stim: ' num2str(kstim(k))])
        [y, fs] = audioread([FolderName listWav(kstim(k)+2).name]); %sample loading

         y = y(:,1); %only left channel used for the subjective test 

       %detection based on wavelet transform
        if ismember(k,idxHIT)
            clickWM(kstim(k)) = vlnkova_segment_mm( y, vlnka, Threshold, LengthFilt,idxLup,idxTol,1);
        else
            clickWM(kstim(k)) = vlnkova_segment_mm( y, vlnka, Threshold, LengthFilt,idxLup,idxTol,0);
        end
    end
elapsed_time = toc; %ulozeni doby behu algoritmu
    
%evaluation
ErrorRate = (1-sum(clickWM(sig_list(idxHIT)))/length(idxHIT))*100; 
CD=100-ErrorRate; %% Hit
FA = (sum(clickWM(sig_list(idxNO)))/length(idxNO))*100; % False Alarm
total = (ErrorRate + FA); 
dprime=(norminv(CD/100)-norminv(FA/100));
    % Ap
cd = CD/100;
fd = FA/100;
Ap = 0.5 + sign(cd-fd)*((cd-fd)^2 + abs(cd-fd))/(4*max(cd,fd)-4*cd*fd);

disp(['Not detected: ' num2str(ErrorRate) '%'])
disp(['Correctly detected: ' num2str(CD) '%'])
disp(['False detected: ' num2str(FA) '%'])
disp(['d prime: ' num2str(dprime) ])
disp(['A prime: ' num2str(Ap) ])
disp(['Time: ' num2str(elapsed_time) ' sec'])
disp('----------------------------')
 
% figure
% bar(idxHIT,clickWM(idxHIT))
% hold on
% bar(idxNO,abs(clickWM(idxNO)))
% plot(idxHIT,subjRes(idxHIT),'-*')
% plot(idxNO,subjRes(idxNO),'-*')
% title('WM')

% rmpath('fce_wavelet');