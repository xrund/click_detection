% Impulse noise detection using ear model
% Lyon model based detection algorithm - evaluation script
% Authors: Marek Semansky, Frantisek Rund
% FEE CTU in Prague
% https://mmtg.fel.cvut.cz/click-detection/

% R. F. Lyon, �A Computational Model of Filtering, Detection and
% Compression in the Cochlea,� v Proceedings of IEEE International
% Conference on Acoustics, Speech and Signal Processing, Paris, 1982

%Lyon needs M. Slaney Auditory Toolbox
%https://engineering.purdue.edu/~malcolm/interval/1998-010/ (tested at ver 2)

function [det, elapsed_time] = JAES_lyon(sada, start_sig, num_sig,threshold_gain,channels_end, sig_list)

% detection parameters

channels = 2:channels_end; %channels (bands) selection
coef_thr = threshold_gain; % 16

tic
addpath('Aud_tbx')
load IR/semansky_lyon_impBM_dec_1.mat %impulse response loading
impBM = impBM(:, 9170:10270)'; %impulse response loading
%det = [];
counter = 0;
listing = dir(sada);
NumSig = size(sig_list,1);
det(90)=0;
for nstim=sig_list
    counter = counter+1;
    %soundsList{counter} = [sada listing(nstim+2).name];
    [y, fs] = audioread([sada listing(nstim+2).name]); %signal loading
    y = y(:, 1); % Limit to the first channel only
%resampling if necessary
    if fs~=44.1e3
        yresamp = resample(y,44.1e3,fs);
        fs = 44.1e3;
    else
        yresamp = y;
    end
    sig_in = yresamp;
%normalization
    Lref = 94;
    Lset = Lref + 20*log10(sqrt(mean(y.^2))*sqrt(2));
    sig_in = 2e-5*10^(Lset/20)*sig_in;


%hearing model applied
    [tempBM] = LyonPassiveEar(sig_in, fs, 1)'; % https://engineering.purdue.edu/~malcolm/interval/1998-010/



%% Detection parameter settings
    % channels = 2:36; %channels (bands) selection
    % coef_thr = threshold_gain; % 16

%correlation
    [Ns, Nch] = size(tempBM);
    [Nimp] = size(impBM,1);
    steps = floor(Ns/Nimp);
    corr_out = [];
    for k=1:steps  
        for ch=1:Nch
            xcoutj(:,ch) = xcorr(impBM(:,ch),tempBM(1+(k-1)*Nimp:k*Nimp,ch), 'coeff');            
        end
        corr_out = [corr_out; xcoutj];
    end

%detection threshold
    threshold = mean(abs(sum(corr_out(:, channels), 2)));

%detection evaluation
    if max(abs(sum(corr_out(:, channels), 2))) > coef_thr*threshold
        det(counter) = 1;
    else
        det(counter) = 0;
    end

end
elapsed_time = toc; %time elapsed
